import 'package:flutter/material.dart';

final kHintTextStyle = TextStyle(
  color: Colors.purple,
  fontFamily: 'OpenSans',
);

final kLabelStyle = TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.bold,
  fontFamily: 'OpenSans',
);

final kBoxDecorationStyle = BoxDecoration(
  color: Colors.white,
  borderRadius: BorderRadius.circular(10.0),
  boxShadow: [
    BoxShadow(
      color: Colors.black12,
      blurRadius: 6.0,
      offset: Offset(0, 2),
    ),
  ],
);

//metodo para mostrar mensajes de alerta
void showMessageDialog(BuildContext context, String message) {
  var alertDialog = AlertDialog(
    title: Text("CATVI"),
    content: Text(message),
  );
  showDialog(context: context, builder: (BuildContext contex) => alertDialog);
}
