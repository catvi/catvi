import 'package:catvi/src/models/chatbot/respuesta.dart';
import 'package:catvi/src/models/db/PersonajeDb.dart';
import 'package:catvi/src/views/menu/MenuPersonajes.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:catvi/src/views/helpers/helpers.dart';

import 'chatbotView.dart';

class Menu extends StatefulWidget {
  var cameras;
  var nombre;
  Menu(this.cameras, this.nombre);
  _menuState createState() => new _menuState();
}

class _menuState extends State<Menu> {
  int _page = 0;
  GlobalKey _bottomNavigationKey = GlobalKey();

  static List<Widget> _listButtomNavigation;

  PersonajeDb _personajeDb;
  int _personajeId;
  Respuesta _respuestas;
  int _inicial;
  @override
  void initState() {
    // Future.delayed(Duration.zero).then((value) {
    //   showMessageDialog(context,
    //       "Recuerda que CATVI recopila datos haciendo uso de la camara y del microfono");
    // });
    _personajeDb = new PersonajeDb();
    _respuestas = new Respuesta();
    _inicial = -1;
    traerPersonaje().then((value) {
      setState(() {
        _personajeId = value;
      });
      _listButtomNavigation = <Widget>[
        DialogScreen(
            widget.cameras, _personajeId, widget.nombre, _respuestas, _inicial),
        MenuPersonajes()
      ];
      _inicial++;
    });

    super.initState();
  }

  void _onTabButtomNavigation(int index) {
    _personajeDb.buscarDatos().then((value) {
      setState(() {
        _personajeId = int.parse(value.toString());
        _inicial++;
        _listButtomNavigation = <Widget>[
          DialogScreen(widget.cameras, _personajeId, widget.nombre, _respuestas,
              _inicial),
          MenuPersonajes()
        ];
        _page = index;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: CurvedNavigationBar(
          key: _bottomNavigationKey,
          index: 0,
          height: 50.0,
          items: <Widget>[
            Icon(Icons.face, size: 30),
            Icon(Icons.list, size: 30),
          ],
          color: Color.fromARGB(255, 165, 51, 255),
          buttonBackgroundColor: Color.fromARGB(255, 165, 51, 255),
          backgroundColor: Colors.white,
          animationCurve: Curves.easeInOut,
          animationDuration: Duration(milliseconds: 600),
          onTap: _onTabButtomNavigation,
        ),
        body: Container(
          color: Colors.blueAccent,
          child: Center(
            child: _personajeId != null
                ? _listButtomNavigation.elementAt(_page)
                : Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Color.fromARGB(255, 208, 208, 208),
                      boxShadow: [
                        BoxShadow(color: Colors.green, spreadRadius: 3),
                      ],
                    ),
                  ),
          ),
        ));
  }

  Future traerPersonaje() {
    return _personajeDb
        .buscarDatos()
        .then((value) => int.parse(value.toString()));
  }
}
