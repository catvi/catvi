import 'dart:math';

import 'package:avatar_glow/avatar_glow.dart';
import 'package:camera/camera.dart';
import 'package:catvi/src/models/chatbot/respuesta.dart';
import 'package:catvi/src/models/db/UsuarioDb.dart';
import 'package:catvi/src/models/dataProcessing/send_data.dart';
import 'package:catvi/src/models/faceDetection/openCamera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gifimage/flutter_gifimage.dart';
import 'package:speech_to_text/speech_to_text.dart' as stt;
import 'package:flutter_tts/flutter_tts.dart';
import '../../models/chatbot/classifier.dart';

// ignore: must_be_immutable
class DialogScreen extends StatefulWidget {
  final List<CameraDescription> cameras;
  final int _personajeId;
  final String _nombreUsuario;
  final Respuesta _respuestas;
  final int _inicial;
  DialogScreen(this.cameras, this._personajeId, this._nombreUsuario,
      this._respuestas, this._inicial);
  @override
  _DialogScreenState createState() => _DialogScreenState();
}

class _DialogScreenState extends State<DialogScreen>
    with TickerProviderStateMixin {
  /**
   * 
   * Atributo de tipo int que gurda el tiempo de pausa que puede hacer uno al escuchar
   * Tiempo en segundos
   */
  static int STOP_DURATION = 10;

  ///Variable que maneja el minimo nivel de sonido que tie
  double _minSoundLevel = 50000;
  double _maxSoundLevel = -50000;
  int _speedGiflisten = 7000;
  int _speedGiftalk = 5000;
  double _frameGiflisten = 39;
  double _frameGiftalk = 39;

  /// Objeto de la clase FlutterTTs para que la aplicación hable
  FlutterTts _chatbotVoice;

  /// Objeto de la clase SpeechToText para reconocer la voz del usuario
  stt.SpeechToText _speech;

  /// Atributo de tipo boolean para detectar si el usuario esta hablando
  bool _isListening = false;

  /// Atributo de tipo String que captura el reconocimiento de voz del usuario
  String _userResponse;

  ///Atributo de tipo double para guardar el nivel de confidencia que tiene la aplicación
  double _confidence;

  /// Atributo de tipo String que captura la respuesta que retorna la aplicación
  String _programResponse;

  /// Atributo de tipo String que maneja el cambio del personaje
  String _character;

  /// Objeto de la clase Classifier para clasificar la respuesta del usuario
  Classifier _classifier;

  ///Objeto de la clase GifController para configurar los cambios del personaje
  GifController gifController;

  ///Objeto de la clase SendData para guardar de la información
  SendData _sendData;

  /// Objeto de la clase OpenCamera para capturar la cara del usuario
  OpenCamera _camera;

  UsuarioDb _usuariodb;

  Respuesta _respuesta;
  int _personajeId;

  int _initial;

  String _nombre = "";
  @override
  void initState() {
    _personajeId = widget._personajeId;
    _respuesta = widget._respuestas;
    _nombre = widget._nombreUsuario;
    gifController = GifController(vsync: this);
    _character =
        "assets/Animaciones/catvi_listen_" + _personajeId.toString() + ".gif";
    _initial = widget._inicial;

    _programResponse = "Hola, bienvenido a CATVI, ¿Como estas?";
    print("inicial $_initial");
    _chatbotVoice = FlutterTts();

    _classifier = new Classifier();

    _sendData = SendData();
    _speech = stt.SpeechToText();
    _confidence = 0.9;
    if (_nombre != null && _nombre != "") {
      _camera = new OpenCamera(widget.cameras, _nombre);
    }

    _userResponse = '';

    _loadTTSConfig();

    // _loadSTTConfig();
    if (_initial == -1 && (_nombre != null && _nombre != "")) {
      _speak().whenComplete(() {
        setState(() {
          _character = "assets/Animaciones/catvi_listen_" +
              _personajeId.toString() +
              ".gif";
          gifController.stop();
        });
      });
    }
    super.initState();
  }

  @override
  void dispose() {
    _chatbotVoice.stop();
    if (_nombre != null && _nombre != "") {
      _camera.stopCapture();
      // _camera = new OpenCamera(widget.cameras, _nombre);
    }
    super.dispose();
  }

  Future _loadSTTConfig() async {
    await _chatbotVoice.setVolume(1);
    await _chatbotVoice.setSpeechRate(1);
    await _chatbotVoice.setPitch(1.6);
  }

/**
 * Metodo que ejecuta el evento de hablar y escuchar de la aplicación
 *Llama al analizador de texto para dar una respuestas
 *  */
  void _listen() async {
    if (!_isListening) {
      setState(() {
        _character = "assets/Animaciones/catvi_listen_" +
            _personajeId.toString() +
            ".gif";
        WidgetsBinding.instance.addPostFrameCallback((_) {
          gifController.repeat(
              min: 0,
              max: _frameGiflisten,
              period: Duration(milliseconds: _speedGiflisten));
        });
      });

      bool available = await _speech.initialize(
        onStatus: (val) => print('Escuchando estatus: $val'),
        onError: (val) {
          print('Escuchando error: $val');
          setState(() {
            _isListening = false;
            gifController.stop();
          });
        },
      );
      if (available) {
        setState(() => _isListening = true);
        _speech.listen(
          onResult: (val) => setState(() {
            _userResponse = val.recognizedWords;

            if (val.hasConfidenceRating && val.confidence > 0) {
              _confidence = val.confidence;
            }
          }),
          pauseFor: Duration(seconds: STOP_DURATION),
          // onSoundLevelChange: soundLevelListener,
        );
      }
    } else {
      print("total preguntas" + _respuesta.totalPreguntas().toString());
      if (!_respuesta.totalPreguntas()) {
        setState(() {
          _isListening = false;

          print("Escuchando $_userResponse");

          int result = _classifier.classify(_userResponse);
          print("resultado $result");
          List<String> respuestas = _respuesta.getResultado(result);
          String resultado_catvi = respuestas[0];
          _programResponse = respuestas[1];
          _sendData.SaveData(
              _nombre, _programResponse, _userResponse, resultado_catvi);
          _userResponse = "";
          _speak().whenComplete(() {
            setState(() {
              gifController.stop();
            });
          });
        });
      } else {
        _programResponse =
            "Gracias por asistir a la cita de hoy, nos vemos luego";
        _speak().whenComplete(() {
          setState(() {
            gifController.stop();
          });
        });
      }

      _speech.stop();
    }
  }

  Future _speak() async {
    setState(() {
      _character =
          "assets/Animaciones/catvi_talk_" + _personajeId.toString() + ".gif";

      gifController.repeat(
          min: 0,
          max: _frameGiftalk,
          period: Duration(milliseconds: _speedGiftalk));
    });
    _chatbotVoice..awaitSpeakCompletion(true);
    var resultado = await _chatbotVoice.speak(_programResponse);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Hola ' + _nombre + ', Habla conmigo'),
        backgroundColor: Color.fromARGB(255, 165, 51, 255),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: AvatarGlow(
        animate: _isListening,
        glowColor: Theme.of(context).primaryColor,
        endRadius: 75.0,
        duration: const Duration(milliseconds: 2000),
        repeatPauseDuration: const Duration(microseconds: 100),
        repeat: true,
        child: FloatingActionButton(
          backgroundColor: Color.fromARGB(255, 165, 51, 255),
          heroTag: null,
          onPressed: _listen,
          child: Icon(_isListening ? Icons.mic_off : Icons.mic),
        ),
      ),
      body: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Color.fromARGB(255, 208, 208, 208),
          boxShadow: [
            BoxShadow(color: Colors.green, spreadRadius: 3),
          ],
        ),
        // color: Colors.white,
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.fromLTRB(0, 0, 0, 0.0),
        child: GifImage(
          controller: gifController,
          image: AssetImage(_character),
        ),
      ),
    );
  }

  /**
   * Metodo que carga las configuraciones del objeto _chatbotVoice
   * Se cofigura 
   */
  Future _loadTTSConfig() async {
    var voices = await _chatbotVoice.getVoices;
    print("voces disponibles" + voices.toString());
    var languaje = await _chatbotVoice.getLanguages;
    print("voces disponibles" + languaje.toString());
    // await _chatbotVoice.setSpeechRate(1);
    await _chatbotVoice.setLanguage('es-US');

    // await _chatbotVoice.setVoice({"cmn-cn-x-ssa-local"});
  }

  void soundLevelListener(double level) {
    _minSoundLevel = min(_minSoundLevel, level);
    _maxSoundLevel = max(_maxSoundLevel, level);
    print("Escuchando level $level: $_minSoundLevel - $_maxSoundLevel ");
  }
}
