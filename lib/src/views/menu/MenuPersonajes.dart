import 'package:catvi/src/models/db/PersonajeDb.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MenuPersonajes extends StatefulWidget {
  _MenuPersonajesState createState() => new _MenuPersonajesState();
}

class _MenuPersonajesState extends State<MenuPersonajes> {
  int _imagenDefault;
  List<String> _personajes;
  int _seleccionado;
  PersonajeDb _personajeNum;

  @override
  void initState() {
    _imagenDefault = 0;
    _personajeNum = new PersonajeDb();
    _personajes = [
      'personaje_0.jpg',
      'personaje_1.jpg',
      'personaje_2.jpg',
      // 'personaje_4.jpg',
    ];
    traerPersonaje();
    // TODO: implement initState

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Selecciona a tu compañero'),
        backgroundColor: Color.fromARGB(255, 165, 51, 255),
      ),
      body: ListView.builder(
          itemCount: _personajes.length,
          itemBuilder: (context, index) {
            return FlatButton(
                onPressed: () {
                  PersonajeDb per = new PersonajeDb.data(1, index.toString());
                  _personajeNum.insertarDatos(per).then((value) => {});
                  setState(() {
                    traerPersonaje();
                    // _imagenDefault = index;
                  });
                },
                child: Padding(
                  padding:
                      EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16.0),
                    ),
                    child: Column(
                      children: [
                        ClipRRect(
                          child: Image.asset(
                            "assets/Personajes/" + _personajes[index],
                            color: index == _imagenDefault
                                ? Colors.purple[100]
                                : Colors.transparent,
                            colorBlendMode: BlendMode.darken,
                          ),
                        )
                      ],
                    ),
                  ),
                ));
          }),
    );
    // TODO: implement build
    throw UnimplementedError();
  }

  void traerPersonaje() {
    _personajeNum.buscarDatos().then((value) {
      setState(() {
        _imagenDefault = int.parse(value.toString());
      });
    });
  }
}
