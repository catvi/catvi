import 'package:catvi/src/models/db/UsuarioDb.dart';
import 'package:catvi/src/views/helpers/helpers.dart';
import 'package:catvi/src/views/login/teminos.dart';
import 'package:catvi/src/views/menu/menu.dart';
import 'package:flutter/material.dart';

class VentanaLogin extends StatefulWidget {
  var cameras;
  VentanaLogin(this.cameras);
  _VentanaLoginState createState() => _VentanaLoginState();
}

class _VentanaLoginState extends State<VentanaLogin> {
  bool _checked = false;
  TextEditingController controllerName = TextEditingController();

  Widget _txtNombre() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Nombre',
          style: kLabelStyle,
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60.0,
          child: TextFormField(
            // obscureText: true,
            controller: controllerName,
            style: TextStyle(color: Colors.purple, fontFamily: 'OpenSans'),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.person_outline,
                color: Colors.purple.shade300,
              ),
              hintText: 'Digita tu nombre',
              hintStyle: kHintTextStyle,
            ),
          ),
        ),
        TextButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Terminos()),
              );
            },
            child: Text("Terminos y condiciones")),
        CheckboxListTile(
            value: _checked,
            title: Text("Acepto los terminos y condiciones"),
            onChanged: (value) {
              _checked = value;
            })
      ],
    );
  }

  Widget _btnOlvidoContra() {
    return Container(
      alignment: Alignment.centerRight,
      child: FlatButton(
        onPressed: () => print('Forgot Password Button Pressed'),
        padding: EdgeInsets.only(right: 0.0),
        child: Text(
          'Forgot Password?',
          style: kLabelStyle,
        ),
      ),
    );
  }

  Widget _btnAcceder(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 25.0),
      width: double.infinity,
      child: RaisedButton(
        elevation: 5.0,
        onPressed: () => _login(context),
        padding: EdgeInsets.all(15.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
        color: Colors.white,
        child: Text(
          'ENTRAR',
          style: TextStyle(
            color: Color.fromARGB(255, 165, 51, 255),
            letterSpacing: 1.5,
            fontSize: 18.0,
            fontWeight: FontWeight.bold,
            fontFamily: 'OpenSans',
          ),
        ),
      ),
    );
  }

  _login(BuildContext context) async {
    print("guardando dato " + controllerName.text);
    UsuarioDb _save = UsuarioDb();
    UsuarioDb usuario = UsuarioDb.data(1, controllerName.text);
    var res = await _save.insertarDatos(usuario);
    if (res != false) {
      print("Estas llamando a la funcion");
      // Navigator.pushReplacementNamed(context, "/menu");
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (contex) => Menu(widget.cameras, controllerName.text)));
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Form(
      child: Stack(
        children: <Widget>[
          Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color.fromARGB(255, 182, 62, 234),
                  Color.fromARGB(255, 191, 88, 236),
                  Color.fromARGB(255, 203, 115, 241),
                  Color.fromARGB(255, 207, 125, 243),
                ],
                stops: [0.1, 0.4, 0.7, 0.9],
              ),
            ),
          ),
          Container(
            height: double.infinity,
            child: SingleChildScrollView(
              physics: AlwaysScrollableScrollPhysics(),
              padding: EdgeInsets.symmetric(
                horizontal: 40.0,
                vertical: 120.0,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.1,
                  ),
                  Text(
                    'Bienvenida a CATVI',
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'OpenSans',
                      fontSize: 30.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(
                    height: 30.0,
                  ),
                  Text(
                    'Digita tu nombre para conocerte mejor',
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'OpenSans',
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 30.0,
                  ),
                  // _txtCorreo(),

                  _txtNombre(),
                  SizedBox(
                    height: 30.0,
                  ),
                  // _btnOlvidoContra(),
                  _btnAcceder(context),
                ],
              ),
            ),
          )
        ],
      ),
    ));
  }
}
