import 'dart:io';

import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class SendData {
  String urlSheet =
      "https://script.google.com/macros/s/AKfycbwiTww4hH2E7aajjIZ1JF6sqdhipTzTk223hmq5IA0V_z1jxaSxLd2jXqtJvHrZVSv-ng/exec";

  // String urlImage =
  // "https://script.google.com/macros/s/AKfycbyrw7UIJoNqEqsdsjETKJq6rTBBvvQxT3MOfKeOHVS2cktD6oEwq8aM_KdRwajQJU5Tqw/exec";
  String urlImage =
      "https://script.google.com/macros/s/AKfycbxpBkxqWHni_vHrvoGo7lV3_nq0T8MQDyB9qqNzqnp0palzYB_9q6TAlllr7Vdr_n-jCA/exec";

  String urlSentimiento = "https://catvi.herokuapp.com/analysis/";
  SendData();

  void SaveData(String paciente, String pregunta_catvi,
      String respuesta_paciente, String resultado_catvi) async {
    try {
      var sendSentiment = await http
          .get(urlSentimiento + "?texto=$respuesta_paciente")
          .then((value) {
        if (value.statusCode == 200) {
          return value.body;
        }
      }).catchError((error) {
        return false;
      });
      if (sendSentiment != false) {
        String analisis_sentimiento = sendSentiment;
        String data =
            "?action=1&paciente=$paciente&pregunta_catvi=$pregunta_catvi&respuesta_paciente=$respuesta_paciente&resultado_catvi=$resultado_catvi&analisis_sentimiento=$analisis_sentimiento";
        var saveData = await http.get(urlSheet + data).then((value) {
          if (value.statusCode == 200) {
            return value.body;
          }
        }).catchError((error) {
          return false;
        });
      }
    } catch (e) {
      print("  resultado  error" + e);
    }
  }

  void saveImage(image, imageName, nameFolder) async {
    var request = {
      "file": image,
      "imageName": imageName,
      "nameFolder": nameFolder
    };
    print("request" + convert.jsonEncode(request).toString());
    print("request" + convert.jsonEncode(request['imageName']).toString());
    var header = {'Accept': "application/json"};
    var response = await http.post(Uri.parse(urlImage),
        headers: header, body: convert.jsonEncode(request));
    print("response" + response.body);
  }
}
