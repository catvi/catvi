import 'dart:async';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:catvi/src/models/faceDetection/faceClasifier.dart';
import 'package:catvi/src/models/dataProcessing/imagePreprocesing.dart';
import 'package:catvi/src/models/dataProcessing/send_data.dart';
import 'dart:convert' as convert;

import 'package:image/image.dart' as imageLib;

class OpenCamera {
  final List<CameraDescription> cameras;

  final String _nombre;
  CameraController _controller;

  ImageClassifier _iclassifier;

  bool _isDetecting;

  bool _perimitions;
  bool _stopRecording;

  int i = 5;

  SendData _sendImage;

  OpenCamera(this.cameras, this._nombre) {
    _sendImage = SendData();

    _isDetecting = false;
    _perimitions = true;
    _stopRecording = false;

    _iclassifier = new ImageClassifier();

    if (this.cameras == null || this.cameras.length < 1) {
      _perimitions = false;
    } else {
      _controller = new CameraController(
        this.cameras[1],
        ResolutionPreset.veryHigh,
      );
      _perimitions = true;

      _controller.initialize().then((_) {
        // if (!mounted) {
        //   return;
        // }
        if (!_stopRecording) {
          _captureImage();
        }
      });
    }
  }

  _captureImage() async {
    await _controller.startImageStream((image) {
      i--;
      bool capture = i < 0;

      if (capture) {
        _isDetecting = false;
        i = 30;
      }
      if (!_isDetecting) {
        var imagenConvert = ImageUtils.convertYUV420ToImage(image);
        var imageSend = ImageUtils.rotateImage(imagenConvert);

        var list = imageLib.encodeJpg(imageSend);

        _sendImage = SendData();
        var encode = convert.base64Encode(list);
        var now = DateTime.now();
        var clasify = _iclassifier.predict(imageSend);
        var name = this._nombre.toString() +
            "_" +
            clasify[0].toString() +
            "_" +
            clasify[1].toString() +
            "_" +
            now.toString();

        var save = _sendImage.saveImage(encode, name, this._nombre);

        _isDetecting = true;
      }
    });
  }

  void stopCapture() async {
    print("camera stop");
    _stopRecording = true;
    try {
      _controller.stopImageStream();
    } catch (e) {
      print("Error deteniendo la camara");
    }
  }
}
