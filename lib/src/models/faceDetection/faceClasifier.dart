import 'dart:ffi';
import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:image/image.dart' as imageLib;
import 'package:tflite_flutter/tflite_flutter.dart';
import 'package:tflite_flutter_helper/tflite_flutter_helper.dart';

/// Classifier
class ImageClassifier {
  /// Instance of Interpreter for happy model
  Interpreter _happyInterpreter;

  /// Instance of Interpreter for sad model
  Interpreter _sadInterpreter;

  /// Instance of Interpreter for angry model
  Interpreter _angryInterpreter;

  /// Labels file loaded as list for happy
  List<String> _happyLabels;

  /// Labels file loaded as list for sad
  List<String> _sadLabels;

  /// Labels file loaded as list for angry
  List<String> _angryLabels;

  static const String HAPPY_MODEL_FILE_NAME = "happy_model.tflite";
  static const String HAPPY_LABEL_FILE_NAME = "happyLabels.txt";

  static const String SAD_MODEL_FILE_NAME = "sad_model.tflite";
  static const String SAD_LABEL_FILE_NAME = "sadLabels.txt";

  static const String ANGRY_MODEL_FILE_NAME = "angry_model.tflite";
  static const String ANGRY_LABEL_FILE_NAME = "angryLabels.txt";

  /// Input size of image (height = width = 300)
  static const int INPUT_SIZE = 180;

  /// Result score threshold
  static const double THRESHOLD = 0.5;

  /// [ImageProcessor] used to pre-process the image
  ImageProcessor imageProcessor;

  /// Padding the image to transform into square
  int padSize;

  /// Shapes of output tensors for happy model
  List<List<int>> _happyOutputShapes;

  /// Shapes of output tensors for sad model
  List<List<int>> _sadOutputShapes;

  /// Shapes of output tensors for angry model
  List<List<int>> _angryOutputShapes;

  /// Types of output tensors for happy
  List<TfLiteType> _happyOutputTypes;

  /// Types of output tensors for sad
  List<TfLiteType> _sadOutputTypes;

  /// Types of output tensors for angry
  List<TfLiteType> _angryOutputTypes;

  /// Number of results to show
  static const int NUM_RESULTS = 10;

  ImageClassifier() {
    loadModel();
    loadLabels();
  }

  /// Loads interpreter from asset
  void loadModel() async {
    try {
      _happyInterpreter = await Interpreter.fromAsset(HAPPY_MODEL_FILE_NAME,
          options: InterpreterOptions()..threads = 4);
      _sadInterpreter = await Interpreter.fromAsset(HAPPY_MODEL_FILE_NAME,
          options: InterpreterOptions()..threads = 4);
      _angryInterpreter = await Interpreter.fromAsset(HAPPY_MODEL_FILE_NAME,
          options: InterpreterOptions()..threads = 4);

      var happyOutputTensors = _happyInterpreter.getOutputTensors();
      var sadOutputTensors = _sadInterpreter.getOutputTensors();
      var angryOutputTensors = _angryInterpreter.getOutputTensors();
      _happyOutputShapes = [];
      _sadOutputTypes = [];
      _angryOutputShapes = [];
      _happyOutputTypes = [];
      _sadOutputShapes = [];
      _angryOutputTypes = [];
      happyOutputTensors.forEach((tensor) {
        _happyOutputShapes.add(tensor.shape);
        _happyOutputTypes.add(tensor.type);
      });

      sadOutputTensors.forEach((tensor) {
        _sadOutputShapes.add(tensor.shape);
        _sadOutputTypes.add(tensor.type);
      });

      angryOutputTensors.forEach((tensor) {
        _angryOutputShapes.add(tensor.shape);
        _angryOutputTypes.add(tensor.type);
      });
    } catch (e) {}
  }

  /// Loads labels from assets
  void loadLabels() async {
    try {
      _happyLabels =
          await FileUtil.loadLabels("assets/" + HAPPY_LABEL_FILE_NAME);
      _sadLabels = await FileUtil.loadLabels("assets/" + SAD_LABEL_FILE_NAME);
      _angryLabels =
          await FileUtil.loadLabels("assets/" + ANGRY_LABEL_FILE_NAME);
    } catch (e) {}
  }

  /// Pre-process the image

  TensorImage getProcessedImage(TensorImage inputImage) {
    padSize = max(inputImage.height, inputImage.width);
    if (imageProcessor == null) {
      imageProcessor = ImageProcessorBuilder()
          .add(ResizeWithCropOrPadOp(padSize, padSize))
          .add(ResizeOp(INPUT_SIZE, INPUT_SIZE, ResizeMethod.BILINEAR))
          .build();
    }
    inputImage = imageProcessor.process(inputImage);
    return inputImage;
  }

  /// Runs object detection on the input image
  List<dynamic> predict(imageLib.Image image) {
    var predictStartTime = DateTime.now().millisecondsSinceEpoch;

    if (_happyInterpreter == null ||
        _sadInterpreter == null ||
        _angryInterpreter == null) {
      return null;
    }

    var preProcessStart = DateTime.now().millisecondsSinceEpoch;

    // Create TensorImage from image
    TensorImage inputImage = TensorImage(TfLiteType.float32);
    inputImage.loadImage(image);

    // Pre-process TensorImage
    inputImage = getProcessedImage(inputImage);

    // // TensorBuffers for output tensors
    TensorBuffer happyOutputLocations =
        TensorBufferFloat(_happyOutputShapes[0]);
    TensorBuffer sadOutputLocations = TensorBufferFloat(_sadOutputShapes[0]);
    TensorBuffer angryOutputLocations =
        TensorBufferFloat(_angryOutputShapes[0]);

    // Inputs object for runForMultipleInputs
    // Use [TensorImage.buffer] or [TensorBuffer.buffer] to pass by reference
    List<Object> inputs = [inputImage.buffer];

    // Outputs map
    Map<int, Object> happyOutputs = {0: happyOutputLocations.buffer};
    Map<int, Object> sadOutputs = {0: sadOutputLocations.buffer};
    Map<int, Object> angryOutputs = {0: angryOutputLocations.buffer};

    var inferenceTimeStart = DateTime.now().millisecondsSinceEpoch;

    // // run inference
    _happyInterpreter.runForMultipleInputs(inputs, happyOutputs);
    _sadInterpreter.runForMultipleInputs(inputs, sadOutputs);
    _angryInterpreter.runForMultipleInputs(inputs, angryOutputs);

    var happyres =
        get_clasify(happyOutputLocations.getDoubleList(), _happyLabels);
    var sadres = get_clasify(sadOutputLocations.getDoubleList(), _sadLabels);
    var hangryres =
        get_clasify(angryOutputLocations.getDoubleList(), _angryLabels);
    List<double> porcentajes = [happyres[0], sadres[0], hangryres[0]];
    var valores = [happyres[1], sadres[1], hangryres[1]];

    var index = porcentajes.reduce(max);
    int indexvalue = porcentajes.indexOf(index);

    var resultado = [index, valores[indexvalue]];
    return resultado;
  }

  List<dynamic> get_clasify(var output, List labels) {
    List<double> value = output;
    double confience = 0.5;
    int choosen_class = 0;
    int indexvalue = value.indexOf(value.reduce(max));
    if (value[indexvalue] > confience) {
      choosen_class = indexvalue;
    }
    var resp = [value[indexvalue], labels.toList()[choosen_class]];

    return resp;
  }

  /// Gets the interpreter instance
  Interpreter get happyInterpreter => _happyInterpreter;

  /// Gets the loaded labels
  List<String> get happyLabels => _happyLabels;

  /// Gets the interpreter instance
  Interpreter get sadInterpreter => _sadInterpreter;

  /// Gets the loaded labels
  List<String> get sadLabels => _sadLabels;

  /// Gets the interpreter instance
  Interpreter get angryInterpreter => _angryInterpreter;

  /// Gets the loaded labels
  List<String> get angryLabels => _angryLabels;
}
