import 'dart:async';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class PersonajeDb {
  int _id;
  String _numero_personaje;

  PersonajeDb();
  PersonajeDb.data(this._id, this._numero_personaje) {
    this._id = _id;
    this._numero_personaje = _numero_personaje;
  }

  Future<bool> insertarDatos(PersonajeDb usuario) async {
    final Future<Database> database = openDatabase(
      join(await getDatabasesPath(), 'personaje_database.db'),
      onCreate: (db, version) {
        print("Bd de usuario creada");
        return db.execute(
          "CREATE TABLE personaje (id int PRIMARY KEY, numero_personaje TEXT)",
        );
      },
      onOpen: (db) => print("Bd abierta"),
      version: 1,
    );
    Future insertNombrePersonaje(PersonajeDb per) async {
      final Database db = await database;

      await db
          .insert(
            'personaje',
            per.toMap(),
            conflictAlgorithm: ConflictAlgorithm.replace,
          )
          .then((value) => print("valor insertado" + value.toString()));
    }

    insertNombrePersonaje(usuario);
  }

  Future<String> buscarDatos() async {
    final Future<Database> database = openDatabase(
      join(await getDatabasesPath(), 'personaje_database.db'),
      onCreate: (db, version) {
        print("Bd de usuario creada");
        return db.execute(
          "CREATE TABLE personaje (id int PRIMARY KEY, numero_personaje TEXT)",
        );
      },
      onOpen: (db) => print("Bd abierta"),
      version: 1,
    );

    Future<List> listarPersonaje() async {
      // Get a reference to the database.
      final Database db = await database;

      // Query the table for all The Dogs.
      final List<Map<String, dynamic>> maps = await db.query('personaje');
      print("tamaño maps " + maps.toString());
      // Convert the List<Map<String, dynamic> into a List<Dog>.
      return maps;
    }

    var numero = await listarPersonaje().then((value) {
      int n = value.length;
      if (n > 0) {
        return value[0]['numero_personaje'];
      } else {
        return "0";
      }
    });
    return numero;
  }

  Map<String, dynamic> toMap() {
    return {
      'id': this._id,
      'numero_personaje': this._numero_personaje,
    };
  }
}
