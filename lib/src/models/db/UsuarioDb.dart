import 'dart:async';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class UsuarioDb {
  int _id;
  String _nombre_usuario;

  UsuarioDb();
  UsuarioDb.data(this._id, this._nombre_usuario) {
    this._id = _id;
    this._nombre_usuario = _nombre_usuario;
  }

  Future<bool> insertarDatos(UsuarioDb usuario) async {
    final Future<Database> database = openDatabase(
      join(await getDatabasesPath(), 'usuario_database.db'),
      onCreate: (db, version) {
        print("Bd de usuario creada");
        return db.execute(
          "CREATE TABLE usuario (id int PRIMARY KEY, nombre_usuario TEXT)",
        );
      },
      onOpen: (db) => print("Bd abierta"),
      version: 1,
    );
    Future insertNombreUsuario(UsuarioDb usu) async {
      final Database db = await database;

      await db
          .insert(
            'usuario',
            usu.toMap(),
            conflictAlgorithm: ConflictAlgorithm.replace,
          )
          .then((value) => print("valor insertado" + value.toString()));
    }

    insertNombreUsuario(usuario);
  }

  Future<String> buscarDatos() async {
    final Future<Database> database = openDatabase(
      join(await getDatabasesPath(), 'usuario_database.db'),
      onCreate: (db, version) {
        print("Bd de usuario creada");
        return db.execute(
          "CREATE TABLE usuario (id int PRIMARY KEY, nombre_usuario TEXT)",
        );
      },
      onOpen: (db) => print("Bd abierta"),
      version: 1,
    );

    Future<List> listarUsuario() async {
      // Get a reference to the database.
      final Database db = await database;

      // Query the table for all The Dogs.
      final List<Map<String, dynamic>> maps = await db.query('usuario');
      print("tamaño maps " + maps.toString());
      // Convert the List<Map<String, dynamic> into a List<Dog>.
      return maps;
    }

    var usuarios = await listarUsuario().then((value) {
      int n = value.length;
      if (n > 0) {
        return value[0]['nombre_usuario'];
      } else {
        return "";
      }
    });
    return usuarios;
  }

  Map<String, dynamic> toMap() {
    return {
      'id': this._id,
      'nombre_usuario': this._nombre_usuario,
    };
  }
}
