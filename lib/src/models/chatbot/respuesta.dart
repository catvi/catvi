import 'dart:math';

class Respuesta {
  List _intents;

  int _ipositivas;

  int _inegativas;

  int _total;
  /**
   * Lista que contiene las Preguntas postivas a realizarle al usuario
   */
  List<String> _preguntas_positivas;
/**
   * Lista que contiene las Preguntas negativas a realizarle al usuario
   */
  List<String> _preguntas_negativas;
  Respuesta() {
    _intents = ['violencia', 'no_violencia'];

    _preguntas_negativas = [
      '¿Cómo fue la relación con tu pareja?'
          '¿Cómo conociste a tu pareja?',
      '¿Tu pareja presentaba cambios de actitud?',
      '¿te culpó de sus cambios de actitud?',
      '¿Por qué continuaste con tu pareja?',
      '¿Cómo te sientes ahora?',
    ];

    _preguntas_positivas = [
      '¿Como te has sentido con el tratamiento hasta ahora?',
      '¿Cómo te has sentido en esta semana?',
      '¿como te sientes con tu pareja?',
      '¿Sientes que tu vida ha vuelto a la normalidad?',
      '¿Crees que has superado tu tratamiento?'
          '¿qué piensas de tu vida ahora?'
    ];
    _ipositivas = 0;
    _inegativas = 0;
    _total = _preguntas_negativas.length;
  }

  /**
   * Metodo que retorna la cantidad de preguntas positivas que lleva el usuario
   */
  int getIPositivas() {
    return _ipositivas;
  }

  /**
   * Metodo que incrementa el valor de la posicion para los positivos
   */
  void incrementarPositivas() {
    _ipositivas++;
  }

  /**
   * Metodo que retorna la cantidad de preguntas negativas que lleva el usuario
   */
  int getINegativas() {
    return _inegativas;
  }

  /**
   * Metodo que incrementa el valor de la posicion para los negativas
   */
  void incrementarNegativas() {
    _inegativas++;
  }

  bool totalPreguntas() {
    return (_ipositivas + _inegativas) == _total;
  }

  List<String> getResultado(int pos) {
    List<String> res = ["falla", "Lo siento no entendí lo que dijiste"];
    switch (pos) {
      case -1:
        return res;
        break;
      case 0:
        res = [_intents[0], _preguntas_negativas[_inegativas]];
        incrementarNegativas();
        return res;
        break;
      case 1:
        res = [_intents[1], _preguntas_positivas[_ipositivas]];
        incrementarPositivas();
        return res;

        break;
      default:
        return res;
        break;
    }
  }
}
