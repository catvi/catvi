import 'package:catvi/src/models/db/usuarioDb.dart';
import 'package:catvi/src/views/login/login_ventana.dart';
import 'package:catvi/src/views/menu/menu.dart';
import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'package:flutter/services.dart';

List<CameraDescription> cameras;
String nombreusuario = "";
Future<Null> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) async {
    try {
      cameras = await availableCameras();
      UsuarioDb _usuariodb = UsuarioDb();
      _usuariodb.buscarDatos().then((value) {
        nombreusuario = value.toString();
        runApp(MyApp());
      });
    } on CameraException catch (e) {
      print('Error: $e.code\nError Message: $e.message');
    }
  });
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'CATVI',
      initialRoute: nombreusuario != "" ? "/" : "/name",
      routes: {
        "/": (context) {
          return Menu(cameras, nombreusuario);
        },
        "/name": (context) => VentanaLogin(cameras)
      },
      // routes: {"/": (context) => MyHomePage(title: 'Flutter Demo Home Page')},
      theme: ThemeData(
        primarySwatch: Colors.purple,
        backgroundColor: Color.fromARGB(255, 165, 51, 255),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}
